﻿using System;
using System.IO;

namespace LOLExecPatcher
{
    class Program
    {
        private static readonly byte[] WAD_PATCH = { 0xB8, 0x01, 0x00, 0x00, 0x00 };
        private static readonly byte[] WAD_PATTERN = { 0x83, 0xE6, 0xE0, 0x56, 0x53, 0x57, 0xE8 };
        private static readonly byte WAD_REPLACE = 0xE8;
        private static string execPath;

        static void Main(string[] args)
        {
            if (args.Length != 1)
            {
                Console.WriteLine("ERROR: You MUST pass 'League of Legends.exe' path as first argument");
                Exit();
            }

            execPath = args[0];
            Console.WriteLine(String.Format("File: {0}", execPath));

            if (!File.Exists(execPath))
            {
                Console.WriteLine(String.Format("ERROR: {0} does not exist", execPath));
                Exit();
            }
 
            using (FileStream fs = new FileStream(execPath, FileMode.Open))
            {
                long pattern = FindPattern(fs, WAD_PATTERN);
                long offset = FindByte(fs, WAD_REPLACE);
                Console.WriteLine(String.Format("Pattern: {0}", pattern));
                Console.WriteLine(String.Format("Offset: {0}", offset));
                Console.WriteLine("PATCHING...");

                if (offset != 0)
                {
                    fs.Seek(offset, SeekOrigin.Begin);
                    fs.Write(WAD_PATCH, 0, WAD_PATCH.Length);
                    fs.Close();
                    Console.WriteLine("\r\nPATCHED!");
                }
                else
                {
                    Console.WriteLine("ERROR: NOT PATCHED, PATTERN NOT FOUND");
                }
            }
        }

        static void Exit()
        {
            // Console.WriteLine("\r\nPress any key to exit...");
            System.Environment.Exit(1);
        }

        static long FindByte(Stream stream, byte toFind)
        {
            byte[] found = new byte[1];
            long currentPosition = stream.Position;

            while (stream.Read(found, 0, 1) == 1)
            {
                if (found[0] == toFind)
                {
                    return currentPosition;
                }

                currentPosition = stream.Position;
            }

            return 0;
        }
  
        static long FindPattern(Stream stream, byte[] pattern)
        {
            byte[] found = new byte[1];
            int matchCount = 0;
            long currentPosition = stream.Position;

            while (stream.Read(found, 0, 1) == 1)
            {
                if (found[0] == pattern[matchCount])
                {
                    matchCount++;

                    if (matchCount == pattern.Length)
                    {
                        return currentPosition;
                    }
                }
                else
                {
                    matchCount = 0;
                }

                currentPosition = stream.Position;
            }

            return 0;
        }
    }
}
